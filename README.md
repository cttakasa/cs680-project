# cs680-project



## A critical analysis of the Bregman Power k-Means clustering model, with use cases ranging from synthetically generated data from various exponential distributions, and a real-world use case.

### The included code allows reproduction of experiments & results shown in the paper.